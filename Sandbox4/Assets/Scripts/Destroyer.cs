﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

    [SerializeField]
    private GameSystem gameSystem;
	
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Destroy(collider.gameObject);
        if (!gameSystem.gameOver)
        {
            ScoreManager.instance.IncrementScore();
        }
    }
}
