﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour {

    public static GameSystem instance;
    public bool gameOver, gameStart;

    private void Awake()
    {
        if(instance = null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        if (gameStart)
        {
            GameStart();
        }
        if (gameOver)
        {
            GameOver();
        }
    }

    private void GameStart()
    {
        ObstacleSpawner.instance.Spawning();
        UImanager.instance.GameStart();
    }


    private void GameOver()
    {
        gameStart = false;
        ScoreManager.instance.StopScore();
        UImanager.instance.GameOver();
    }
}
