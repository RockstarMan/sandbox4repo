﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControler : MonoBehaviour {


    private Rigidbody2D playerBody2D;
    private bool firstPress, grounded;
    [SerializeField]
    private GameSystem gameSystem;
    private bool left, right;

    public float force;
    public bool startGame;

    private void Awake()
    {
        firstPress = true;
        grounded = true;
        startGame = false;
        right = false;
        left = false;
    }

    // Use this for initialization
    void Start()
    {
        playerBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0) && grounded)
        {
            Movement();
        }
    }

    public void Movement()
    {
        grounded = false;
        

        if (firstPress)
        {
           
            float randomNumber = Random.Range(0, 2);

            if (randomNumber == 0)
            {
                left = true;
            }
            else if (randomNumber == 1)
            {
                right = true;
            }

            firstPress = false;
            gameSystem.gameStart = true;

        }

        if (right)
        {
            
            playerBody2D.AddForce(new Vector2(-force *Time.deltaTime, 0));
            left = true;
            right = false;

        }
        else if (left)
        {
            playerBody2D.AddForce(new Vector2(force*Time.deltaTime, 0f));
            right = true;
            left = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            grounded = true;
        }

        if(collision.gameObject.tag == "Obstacle")
        {
            gameSystem.gameOver = true;
            grounded = false;
            playerBody2D.velocity = collision.relativeVelocity;
        }
    }
}
