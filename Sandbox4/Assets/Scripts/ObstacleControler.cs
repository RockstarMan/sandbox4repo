﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleControler : MonoBehaviour {

    private Rigidbody2D obstacleBody;

    public float speed;

	// Use this for initialization
	void Start () {
        obstacleBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        obstacleBody.velocity = Vector2.down * speed * Time.deltaTime;
    }     
}
