﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {

    public static ObstacleSpawner instance;
    public GameObject obstacle1, obstacle2;
    public float spawnRate;

    private float time;

    public void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void Spawning()
    {
        time += 1 * Time.deltaTime;

        if (time >= spawnRate)
        {
            SpawnObstacle();
        }
    }

    private void SpawnObstacle()
    {
        float randomNumber = Random.Range(1, 3);
        if(randomNumber == 1) {
            Instantiate(obstacle1, obstacle1.GetComponent<Transform>().position, Quaternion.identity);
        }
        else if(randomNumber == 2)
        {
            Instantiate(obstacle2, obstacle2.GetComponent<Transform>().position, Quaternion.identity);
        }
        time = 0f;
        
    }
}
