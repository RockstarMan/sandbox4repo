﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager instance;
    public Text scoreText;

    private int score;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        score = 0;
    }

    private void Start()
    {
        PlayerPrefs.SetInt("Score", 0);
    }

    private void Update()
    {
        scoreText.text = score.ToString();
    }

    public void IncrementScore()
    {
        score++;
    }

    public void StopScore()
    {
        PlayerPrefs.SetInt("Score", score);
        if (PlayerPrefs.HasKey("HighScore"))
        {
            if(score> PlayerPrefs.GetInt("HighScore"))
            {
                PlayerPrefs.SetInt("HighScore", score);
            }
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
    }
}
