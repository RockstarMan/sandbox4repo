﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UImanager : MonoBehaviour {

    public static UImanager instance;
    public Text tapToStartText;
    public Text highscore;
    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void ResetButton()
    {
        SceneManager.LoadScene(0);
    }

    public void GameStart()
    {
        tapToStartText.gameObject.SetActive(false);
    }

    public void GameOver()
    {
        highscore.text = "HIGH SCORE:\n" + PlayerPrefs.GetInt("HighScore");
    }
}
